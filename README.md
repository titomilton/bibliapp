# Bibliapp

## Pré-requisitos

* NodeJS

## Uso

* Baixe este repositório: `git clone https://github.com/titomilton/bibliapp.git`
* Entre no seu diretório: `cd bibliapp`
* `npm install`
* `npm start`
* Aplicação ficará disponível em `http://localhost:8000`
